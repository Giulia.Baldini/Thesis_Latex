\chapter{Approaches}
\label{approach}

In this chapter we present the approaches we considered to find semantic similarity between adjectives. We start mentioning how we cleaned the input data. Then, we will go through the lexical approaches, that is, the ones that use the lexical database \wordnet. In the following section we describe the word-vector methods in which we use \spacy. Finally, we present some techniques that allow our approaches to be integrated into the \toknow system.
\section{Identifying Features and Adjectives}
\label{approach:preprocessing}
In the context of opinionated triples, at the beginning the data had a total of 71,487 features and 28,1505 triples, which results in an average of 4 triples per feature. This very low number is due to the fact that many features are misspelled, and thus, they are not recognised as the same word.
In addition to this issue, we also wanted to speed up the synonym finding process, so we decided to perform \emph{triple cleansing}.\\
We started by checking if the features were words in \wordnet, so that we could leave out the spelling errors, the presence of symbols and non-existing words. After this first action, the features were down to 4,817. After that, we checked if all opinions were adjectives. After this last check, there were 595 features left, with an average of 111 triples per feature.\\
After this process, all the triples left are of the form \triple{feature}{adjective} or \triplenot{feature}{adjective} and so are stored in the \toknow index.

\section{Finding Synonyms and Similarities}
\label{approach:syns}
Here we present all the approaches we used to find synonyms and similar words. Since our aim is to find either synonyms or similarities, we use these words indistinctly. We start by describing first the ones generated with \wordnet and then we move on to the ones captured with \spacy. For each of these, we are going to give a name with which we will label them throughout the entire thesis.\\
In general, we use a pretty heuristic approach; we show all our ideas and then we evaluate them against a ground truth, that in our case is the result of a survey. We define \emph{similarity triples} the triples resulted from one of our similarity functions. In fact, these functions do not return the synonyms of an adjective, but well-formed triples of the form \triple{feature}{synonym\_of\_adjective}.
\vspace{0.5em}

In general, our approach takes as input all the triples contained in the dataset and the queried triple, which can be divided into its feature and its adjective. We mention once again that all the triples are of the form \triple{feature}{adjective}. The triples are stored in a dictionary data structure, where each key is a feature and its value is a list of all the triples having that feature as first term. The first step is to retrieve these triples for the feature concerned. Then, in each of the methods, which we describe later, we use a different \emph{synonym\_function} to retrieve the synonyms. The input of this function varies on the method, but in general the feature is considered only by the methods that take context into account. It returns a list of triples of the form \triple{feature}{synonym\_of\_adjective}, which is then intersected with the triples existing in the data for that feature. It would have been possible to implement this intersection inside of the \emph{synonym\_function}, but we decided to keep these functions free from the data, so that they could be re-used for other purposes too. However, for one method this step is done inside of the \emph{synonym\_function} because this method is very slow at retrieving and giving it a smaller dataset helps speeding up the computations. This procedure is explained in \autoref{algo:1}. The inputs between brackets in the \emph{synonym\_function} show the parameters that are taken into account only by some methods, and will be explained later.

\begin{algorithm} 
	\caption{Given the existing triples in the data, the feature and the adjective of the queried triple, it returns a list of triples of the form \triple{feature}{synonym\_of\_adjective} if they are present in the data and retrieved by the \emph{synonym\_function}.}
	\label{algo:1}
	\begin{algorithmic}[1]
		\Function{return\_synonyms}{$triples$, $feature$, $adj$}
		\State $triples\_feature$ = $triples$ [$feature$]
		\State $similarity\_triples$ = \emph{synonym\_function}($adj$, ($feature$), ($triples\_feature$), ($threshold$))
		\State \Return $similarity\_triples$ $\cap$ $triples\_feature$
		\EndFunction
	\end{algorithmic}
\end{algorithm}

An interesting fact about the triples we are considering is that they are not only positive, but also negative. We consider \triplenot{feature}{adjective} and \triple{feature}{antonym(adjective)} as synonyms, since they express the same feeling, as in \triple{room}{quiet} and \triplenot{room}{noisy}. Thus, we will not discuss a different way to treat these triples and will use the most effective method together with \wordnet's antonym function to retrieve their similarity triples.

\subsection{Lexical Approaches}
\label{syns:lexical}
The lexical approach involves the use of a lexical database, \wordnet. We choose to use this tool because its synsets represents synonyms, which are the base of our research. Going back to \autoref{fig:wordnettree}, we are going to concentrate on the \emphq{see also}, \emphq{similar to} and \emphq{antonym} attributes. Since we are looking for similarity between adjectives, we do not consider the \emphq{attribute} and the \emphq{derivationally related forms} useful.\\
Since \wordnet acts as a dictionary, it is not possible to discover to which synset a particular triple belongs. Thus, in all our approaches with this technology we consider all the possible synsets and, as mentioned before, the context is then given by the existing data.\\
We now show different approaches in which we capture different levels of synsets and attributes.\\
Let us consider:
\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eq4.1}
	lemmas\_syns(S_{i}) = lemmas(S_{i} \cup see\_also(S_{i}))
	\end{align}
\end{ceqn}
\end{linenomath*}
\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eq4.2}
	lemmas\_syns\_simil(S_{i}) = lemmas(S_{i} \cup see\_also(S_{i}) \cup similar\_to(S_{i}))
	\end{align}
\end{ceqn}
\end{linenomath*}
The approaches we analyse are:
\begin{enumerate}
\item \unsyn: union of all the lemmas of the synsets and of their \emphq{see also}.
\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eq4.3}
	\bigcup\limits_{i=1}^{n\_synsets} lemmas\_syns(S_{i})
	\end{align}
\end{ceqn}
\end{linenomath*}
\item \unsynsim: union of all lemmas of the synset, their \emphq{see also} and their \emphq{similar to}.
\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eq4.4}
	\bigcup\limits_{i=1}^{n\_synsets} lemmas\_syns\_simil(S_{i})
	\end{align}
\end{ceqn}
\end{linenomath*}
\item \unsynsyn: find the lemmas of the synsets of all the lemmas of \autoref{eq4.1} and then unite them with \autoref{eq4.1} itself.

\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eq4.5}
	\bigcup\limits_{i=1}^{n\_synsets} lemmas(synsets(lemmas\_syns(S_{i}))) \cup lemmas\_syns(S_{i})
	\end{align}
\end{ceqn}
\end{linenomath*}
\item \unsynsynsim: equivalent to the third one, but considering \autoref{eq4.2}.

\begin{linenomath*}
\begin{ceqn}
	\begin{align}
	\label{eq4.6}
	\bigcup\limits_{i=1}^{n\_synsets} lemmas(synsets(lemmas\_syns\_simil(S_{i}))) \cup lemmas\_syns\_simil(S_{i})
	\end{align}
\end{ceqn}
\end{linenomath*}
\end{enumerate}

We could consider larger and larger sets, but we did not think it was meaningful because the more we consider synsets of higher degree, the more we distance ourselves from the original word.

\subsection{Word-Vector Approaches}
\label{syns:wordvectors}
We use \spacy for our word-vector approaches because it is a tool for NLP based on the latest research, implemented to be used in real products and that already contains pre-trained models for word vectors. We are going to use its largest model to find similarity between adjectives.\\
The \spacy vocabulary contains all the words retrieved by the text it is trained on, and since the vocabulary used in reviews is pretty straight-forward, we can assume that
\textit{vocabulary\_reviews} $\subseteq$ \textit{vocabulary\_spacy}.\\
The algorithm for the first of our approaches, \spacya, is described in the following lines. Given the adjective of the triple and a threshold for the cosine similarity, we first find the vector associated with the adjective using \spacy. Then, we find the list of antonyms for all synsets with the help of \wordnet. Afterwards we go through the \spacy vocabulary and for each \textit{word} we check: (1) if the \textit{word} is an adjective (with \wordnet), (2) if it is not in the list of antonyms and (3) if the cosine similarity between the vector of the current \textit{word} and the vector of the adjective has a score above a certain threshold. If all apply, the \textit{word} is converted to a triple of the form \triple{feature}{\textit{word}} and added to the found synonyms. The necessity of finding the antonyms and pruning them from the final result arises because word vectors only take into account context and not polarities. Therefore, if a sentence appears with both \emphq{good} and \emphq{bad}, both will appear in the results.\\
Logically we would consider two vectors similar if $0.5 \leq cos(\theta) < 1$. To find the best threshold among those values we evaluated them against the ground truth (\autoref{evaluation:comparison}) and found that the most suitable one is 0.7. This value will be used for all the \spacy approaches.

\begin{algorithm}
	\caption{Given the adjective of the triple and a threshold for the cosine similarity, it returns a list of triples of the form \triple{feature}{synonym\_of\_adjective} if the cosine similarity between the adjective and its possible synonyms is greater or equal to the threshold.}
	\label{algo:2}
	\begin{algorithmic}[1]
		\Function{synonym\_function\_\spacy}{$adj$, $threshold$}
		\State $similarity\_triples$ = []
		\State $vector\_adj$ = vector($adj$)
		\State $antonyms$ = antonyms($adj$)
		\ForEach {$word \in spacy\_vocabulary$}
		\State  $vector\_word$ = vector($word$)
		\If{adjective($word$) \textbf{and} $word \notin antonyms$} 
		\If{similarity($vector\_adj$, $vector\_word$) $\ge$ $threshold$}
		\State $similarity\_triples$ += to\_triple($word$)
		\EndIf
		\EndIf
		\EndFor
		\State \Return $similarity\_triples$
		\EndFunction
	\end{algorithmic}
\end{algorithm}

The second approach, namely \spacyone, is a slight modification of the previously mentioned one. From this approach on we try to take context into account. The intuition is that if we want our target adjective to be in a certain context, we can try to use a vector that takes us closer to that specific context.\\
Considering our standard triple \emphq{feature is adjective}, we add a new input to \autoref{algo:2}, that is the sum of the vector of the feature and of the vector of the adjective:
\begin{linenomath*}
	\begin{ceqn}
		\begin{align}
		\label{eqscale1}
		vector(\spacyone) = vector(feature) + vector(adjective)
		\end{align}
	\end{ceqn}
\end{linenomath*}
We will always simplify the view of our vectors to two dimensions. Visually, the sum is obtained by the \emph{parallelogram law}. We can draw the vectors such that their initial points coincide and then complete the parallelogram. The resulting diagonal between the initial point and its opposite is the sum of the two vectors (\autoref{fig:sumvectors}).
\begin{figure}
	\centering
	\begin{tikzpicture}[yscale=0.45, xscale=0.45]
	\draw[thin,gray!40] (-7,-7) grid (7,5);
	\draw[<->] (-7,-4)--(7,-4) node[right]{$x$};
	\draw[<->] (-5,-7)--(-5,5) node[above]{$y$};
	\draw[line width=2pt,colorone,-stealth](-5,-4)--(-2,1) node[label={[label distance=0.1cm]200:$\boldsymbol{breakfast}$}]{};
	\draw[line width=2pt,colortwo,-stealth](-5,-4)--(1,-3) node[label={[label distance=0.2cm]230:$\boldsymbol{good}$}]{};
	
	\draw[dotted, line width=2pt,colorone,-stealth](-2,1)--(4,2);
	\draw[dotted, line width=2pt,colortwo,-stealth](1,-3)--(4,2);
	
	\draw[line width=2pt,colorfour,-stealth](-5,-4)--(4,2) node[label={[label distance=0.05cm]120:$\boldsymbol{good + breakfast}$}]{};
	\end{tikzpicture}
	\caption{Representation of the sum of the vectors \emphq{good} and \emphq{breakfast} in two dimensions.}
	\label{fig:sumvectors}
\end{figure}

Mathematically, it is calculated by adding the correspondent components of the vectors. Since all vectors have the same size of 300, considering the vectors $A = (a_1, a_2, \dots, a_{300})$ and $B = (b_1, b_2, \dots, b_{300})$ the equation is as follows:
\begin{linenomath*}
	\begin{ceqn}
		\begin{align}
		\label{eqscale2}
		\vec{A} + \vec{B} = (a_1 + b_1, a_2 + b_2, \dots, a_{300} + b_{300})
		\end{align}
	\end{ceqn}
\end{linenomath*}
However, this would work for any vector size.\\
Going back to \autoref{algo:2}, this approach uses the sum of vectors as a comparison for similarity instead of using the vector of the \textit{word}.
\vspace{0.5em}

The \spacytwo approach applies the principle just explained to both the input vector and the adjectives in the data. Therefore, the sum of vectors of feature and adjective is computed for the input triple and also for the same feature and the current \textit{word} taken into account. In \autoref{algo:3} it is possible to see that the algorithm works in the same way as before, but now it computes the similarity between the two sums of vectors and this might take us even closer to the context in which we want the target adjective to appear.
\begin{algorithm}
	\caption{Given the triple's adjective and feature and a threshold for the cosine similarity, it computes the sum of vectors for adjective and feature and compares them with the sum of the current \textit{word} in the \spacy vocabulary and the feature. It returns a list of triples of the form \triple{feature}{synonym\_of\_adjective} if they are similar according to the threshold.}
	\label{algo:3}
	\begin{algorithmic}[1]
		\Function{synonym\_function\_SpaCy\_sums}{$adj$, $feature$, $threshold$}
		\State $similarity\_triples$ = []		
		\State $vector\_adj$ = vector($adj$)
		\State $vector\_feature$ = vector($feature$)
		\State $sum\_adj\_feature$ = $vector\_adj$ + $vector\_feature$
		\State $antonyms$ = antonyms($adj$)
		\ForEach {$word \in spacy\_vocabulary$}
		\State $vector\_word$ = vector($word$)
		\If{adjective($word$) \textbf{and} $word \notin antonyms$} 
		\State $sum\_word\_feature$ = $vector\_word$ + $vector\_feature$
		\If{similarity($sum\_adj\_feature$, $sum\_word\_feature$) $\ge$ $threshold$}
		\State $similarity\_triples$ += to\_triple($word$)
		\EndIf
		\EndIf
		\EndFor
		\State \Return $similarity\_triples$
		\EndFunction
	\end{algorithmic}
\end{algorithm}

In the context approaches, another idea is to take the intersection between the closest adjectives to our original adjective and the ones closest to the feature. We call this method \spacyinter. The result set is then:
\begin{linenomath*}
	\begin{ceqn}
		\begin{align}
		\label{eqscale3}
		synonyms = synonym\_function\_SpaCy(adjective) \cap synonym\_function\_SpaCy(feature)
		\end{align}
	\end{ceqn}
\end{linenomath*}
The \emph{synonym\_function\_SpaCy} used in this case is the same of \autoref{algo:2}, but in\\ $synonym\_function\_SpaCy(feature)$ it uses the feature instead of the adjective for the comparison.
\vspace{0.5em}

In our last method we want to consider each triple as an entity so that we can measure the similarity between entities. In \spacy it can be achieved using documents. In the \spacy API a document is a \emph{sequence of words, punctuation symbols, whitespaces etc.} Documents have a similarity method that estimates the semantic similarity (cosine similarity) of the words contained in them. We achieve this with the \spacydocs method. In \autoref{algo:4} we show how the similarity of documents is computed. We create a document for the original triple and for each triple that exists in the data. The \emphq{to\_sentence} method transforms a triple of the type \emphq{breakfast is good} in \emphq{good breakfast}. Then, the similarity between the documents is compared with a threshold. As you can see, this method loops over the already existing triples in the data and not the \spacy vocabulary like the previous ones. The reason is that this method is very slow because of the document creation step, and cutting down the number of iterations enhances its performance.

\begin{algorithm}
	\caption{Given a triple, the existing triples in the data and a threshold for the cosine similarity, it returns a list of triples of the form \triple{feature}{synonym\_of\_adjective} if the documents produced from the original triple and the ones generated with the existing triples are similar according to the threshold.}
	\label{algo:4}
	\begin{algorithmic}[1]
		\Function{synonym\_function\_SpaCy\_docs}{$triple$, $triples\_for\_feature$, $threshold$}
		\State $similarity\_triples$ = []
		\State $sentence1$ = to\_sentence($triple$)
		\State $doc1$ = Document($sentence1$)
		\ForEach {$current\_triple \in triples\_for\_feature$}
		\State $sentence2$ = to\_sentence($current\_triple$)
		\State $doc2$ = Document($sentence2$)
		\If{similarity($doc1$, $doc2$) $\ge$ $threshold$}
		\State $similarity\_triples$ += to\_triple($word$)
		\EndIf
		\EndFor
		\State \Return $similarity\_triples$
		\EndFunction
	\end{algorithmic}
\end{algorithm}


\section{Expanding and Ranking Adjectives According to Intensities}
\label{approach:rank}
In the previous section we consider all synonyms to be equal. Here, we show an expansion approach that does not consider them equal, but instead uses scales.
A scale is a sequence of adjectives that could be applied to the same feature. 
Ideally, considering a scale \emphq{good, decent, excellent} we would like to order these adjectives such that the positiveness of the previous one is always less than the positiveness of the following one. This produces \emphq{decent, good, excellent}. If we introduce negative words and keep the same rules, we could get something like \emphq{abominable, bad, decent, good, excellent}.\\
To achieve this, we use two different approaches: \sentiwordnet and \wordvec. We choose \sentiwordnet because it provides a ordering scheme based on sentiment. Since the adjectives we consider always take an opinion into account, we think that this approach is reasonable. The choice of \wordvec relies on the fact that it allows analogy queries and it is possible to use it to get something comparable, such as \emphq{Rome is to Italy like Paris is to\dots?}.

\subsection{Ranking with Polarities}
\label{intensities:sentiwordnet}
As mentioned before, \sentiwordnet assigns three scores to each synset. However, we cannot know which of the synsets is the one we intend to use for query expansion. In this context, Poggio \citeal{Poggio2017} managed to create an algorithm that calculates the weighted polarity for the synsets of a word. According to this scheme, the ones with negative polarity are indicated with a negative number, while the ones with positive polarity with a positive one. To achieve this, he first computes the $synset\_polarity = positive\_score - negative\_score$, and then for each word the average of their synset polarities weighted by their sense number (i.e. the frequency of that synset) such that the more frequent synsets weigh more.\\
Now we can order them by their weighted average score of polarities, which yields a scale from the most negative to the most positive adjective.\\
To generate some adjective scales we did the following:
\begin{itemize}
	\item Choose an adjective $adj$ for which we want the scale and a scale size $n$
	\item Generate $\frac{n}{2} - 1$ similar terms with the help of \wordvec, avoiding antonyms.
	\item Find one antonym for $adj$ with \wordnet.
	\item Generate $\frac{n}{2} - 1$ similar terms for the antonym with the help of \wordvec, avoiding the words of the previous set.
	\item Order the scale according to the previously described method.
\end{itemize}
In this case we use \wordvec and its \emph{most\_similar} function to generate the \emph{neighbourhood} of the adjective and the \emph{neighbourhood} of its antonym. The \textit{most\_similar} function takes as input one word and returns all the closest words (that are adjectives) to the word vector of that word in descending order. Thus, the closest one is the first one appearing in the list. In practice we consider lists of 10 adjectives meaningful for our research, so we look for five positive ones and five negatives ones.\\
However, \wordvec has to take a model as an input. Very famous is the \google. It includes word vectors for a vocabulary of 3 million words and phrases that are trained on roughly 100 billion words from a Google News dataset. We also train our own model on the reviews of our dataset for Rome.\\
Since we have both models, we generate scales for both of them, and then merge them: in this way we can have a scale with a more averaged position of the adjectives and more words. In fact, the merged scale has as position the average of the positions of that word in the two scales.\\
For example, we select the adjective \emphq{adequate} and a scale size of 10.
We start first with the \google, and we find four adjectives in the neighbourhood of \emphq{adequate}:
\begin{center}
	sufficient, insufficient, ample, necessary
\end{center}
Then, we find with \wordnet one antonym for \emphq{adequate}, that is \emphq{inadequate}, and we find its neighbourhood:
\begin{center}
	deficient, ineffective, unsatisfactory, inefficient
\end{center}
Now we have 10 adjectives: eight from the neighbourhoods, \emphq{adequate} and \emphq{inadequate}.\\
We now combine them by looking at their \sentiwordnet scores and obtain:
\begin{center}
	\scale{insufficient, sufficient, deficient, inadequate, unsatisfactory, ineffective, inefficient, adequate, ample, necessary}
\end{center}
We do again the same for the reviews model, and we obtain the following scale:
\begin{center}
	\scale{insufficient, sufficient, deficient, inadequate, ineffective, dysfunctional, adequate, serviceable, functional, satisfactory}
\end{center}

We can now merge the two scales calculating the average of their position and obtain the following scale:
\begin{center}
	\scale{insufficient, sufficient, deficient, inadequate, unsatisfactory, ineffective, dysfunctional, inefficient, adequate, serviceable, ample, functional, satisfactory, necessary}
\end{center}

\subsection{Ranking with Word Vectors}
\label{intensities:word2vec}
As far as we are concerned, the real power of word vectors is predicting a relation between two words given an input relation of two other words. A very common example is \emphq{Man is to king like woman is to..?} In this case most of the word-vector models returns \emphq{queen}. In the vector world, this can be visualised as $king - man + woman = queen$, which involves projection of vectors and then finding the closest word to the \textit{expected point} where the word should be (\autoref{fig:kingqueen}).

\begin{figure}
	\centering
	\begin{tikzpicture}[yscale=0.35, xscale=0.35]
	\draw[thin,gray!40] (-8,-8) grid (8,8);
	\draw[<->] (-8,-6)--(8,-6) node[right]{$x$};
	\draw[<->] (-6,-8)--(-6,8) node[above]{$y$};
	
	\draw[line width=1pt,colortwo,-stealth](-6,-6)--(-5,2) node[label={[label distance=0.2cm]210:$\boldsymbol{man}$}]{};
	
	\draw[line width=1pt,colorone,-stealth](-6,-6)--(-2,6) node[label={[label distance=0.1cm]200:$\boldsymbol{king}$}]{};
	
	\draw[dotted, line width=2pt,colortwo,-stealth](-2,6)--(-3,-2) node[label={[label distance=0.8cm]80:$\boldsymbol{-man}$}]{};
	
	\draw[dotted, line width=2pt,colorthree,-stealth](-3,-2)--(6,0) node[label={[label distance=0.5cm]180:$\boldsymbol{+woman}$}]{};
	
	\draw[line width=1pt,colorfour,-stealth](-6,-6)--(6,0) node[label={[label distance=0.2cm]270:$\boldsymbol{queen}$}]{};

	\draw[line width=1pt,colorthree,-stealth](-6,-6)--(3,-4) node[label={[label distance=0.1cm]270:$\boldsymbol{woman}$}]{};
	
	\end{tikzpicture}
	\caption{Visualisation in two dimensions of vector sum and difference to retrieve \emphq{queen}.}
	\label{fig:kingqueen}
\end{figure}

We call the following approach \emph{scale learning}. Assume that we use the scale \emphq{abominable, bad, decent, good, excellent} and we want to find an equivalent scale for \emphq{friendly}. We select \emphq{good} as seed word and we ask the system \emphq{good is to excellent like friendly is to\dots?} and it returns a word that has the same relation as excellent has to good. If we do this for each term in our reference scale, we obtain a new scale that refers to \emphq{friendly} in the same way \emphq{good} refers to the reference scale.\\
To create a reference scale we used the \sentiwordnet approach with the adjective \emphq{good} and its antonym \emphq{bad}. From this scale we generate the others. Here follows our reference scale:
\begin{center}
	\scale{horrific, dreadful, terrible, bad, reasonable, decent, good, nice, superb, excellent}
\end{center}
Also in this case we use both the \google and the reviews models and then merge the result. Looking at the example \emphq{adequate} from before, we use the above reference scale to generate the Google News scale:
\begin{center}
	\scale{horrifying, inadequate, inadequate, inadequate, sufficient, sufficient, adequate, sufficient, sufficient, sufficient}
\end{center}
Then, we generate the reviews scale:
\begin{center}
	\scale{atrocious, inadequate, atrocious, objectionable, modest, sufficient, adequate, serviceable, exquisite, sufficient}
\end{center}
And finally we merge them:
\begin{center}
	\scale{horrifying, atrocious, inadequate, objectionable, modest, adequate, sufficient, serviceable, exquisite}
\end{center}
In the scale learning approach the merging of scales helps us dispose of duplicates. In these scales we use \emphq{good} as seed word, so each comparison is always of the type \emphq{good is to horrific like adequate is to\dots?} and the term returned is always inserted at the position that \emph{horrific} has in the reference scale. This is repeated for all terms in the scale.

\section{Integrating Synonyms into Queries}
\label{approach:expansion}
The purpose described from the start is to relieve people from formulating complicated queries, and let the system instead do so. To integrate our methods into the system we have two proposals:
\begin{itemize}
	\item Use the best of our methods from \autoref{approach:syns} to extend the original query as if the synonyms were just other occurrences of the queried terms.
	\item Create a scale of adjectives starting from the queried one with one of the previous approaches.
\end{itemize}
Due to time constraints we cannot present any practical results for these approaches. However, both of them rely heavily on the quality of the retrieved synonyms. Therefore, the good results for the synonym retrieval (\autoref{evaluation:comparison}) indicate, that the results for our query expansion approaches should also be favorable

\subsection{Expanding with Synonyms}
\label{exp:syns}
Assume that \emph{synonym\_function(\triple{location}{stunning})} returns \triple{location}{beautiful}, \triple{location}{marvelous}, \triple{location}{good} and \triplenot{location}{ugly}. If we retrieve the reviews to which these triples belong, we still would not have a way to order them. Ideally, we would like to have all occurrences of the synonyms of an adjective substituted by the adjective itself, because we consider all of them as equal. The ToKnow system uses the \emph{Term Frequency-Inverse Document Frequency (\tfidf)} score for each possible combination of term and document, where in our case each term is a triple and each document is a review. This score is a measurement of how important a term is in a document, which we adjust in such way that the resulting new score \tfidfp score measures the importance of a triple \emph{and} all its similarity triples in the document. The original \tfidf is the product of the weighted term frequency and the inverse document frequency:
\begin{linenomath*}
    \begin{ceqn}
        \begin{align}
        \label{tfidf}
        \tfidf{(t,d)}= \tf{(t,d)} \cdot \idf(t)
        \end{align}
    \end{ceqn}
\end{linenomath*}
Assume that $tf(t,d)$ is the number of occurences of term $t$ in document $d$, then for $tf(t,d) > 0$ we define the weighted term frequency $\tf{(t,d)} = 1 + \log tf(t,d)$, and $\tf{(t,d)} = 0$ otherwise. Therefore higher values of $\tf{(t,d)}$ imply a higher frequency of $t$ in $d$. The value $\idf(t)$, on the other hand, measures how important a term is in relation to the entire corpus of documents, giving lower scores to terms that occur in many documents. Let $N$ be the total number of documents and $df(t)$ the number of documents that contain term $t$, then we get:

\begin{linenomath*}
    \begin{ceqn}
        \begin{align}
        \label{idf}
        \idf(t)= \log{\left(\frac{N}{df(t)}\right)}
        \end{align}
    \end{ceqn}
\end{linenomath*}

In both $\tf(t,d)$ and $\idf(t)$ a logarithmic scale is used, such that only large differences in frequencies lead to significant changes in scores. Also, we want to mention that, in the special case that a term occurs in all documents (i.e. $df(t) = N$), the value $\idf(t)$ and therefore $\tfidf(t,d)$ is 0. \\
Now we use the scores to define our new \tfidfp score. Our \tfidfp calculates the score for a virtual set of documents where we replaced all the synonyms with the original adjective, without actually substituting, but computing it over the existing data. Taking the previous example of \triple{location}{stunning}, we virtually substitute \triple{location}{beautiful}, \triple{location}{marvelous}, \triple{location}{good} and \triplenot{location}{ugly} with \triple{location}{stunning} itself. We now make two reasonable assumptions: We assume, that there are no triples that occur in all documents (i.e. there is no term with $\idf(t) = 0$, that is, no term that occurs in all documents). Also, let $S = \{s_1,\dots,s_n\}$ be the set of synonym terms of $t$, we do not expect two triples that are synonyms to appear in the same review (i.e. there cannot be $\sum_{s \in S}df(s) > N$). We assume that this is reasonable because overlaps in reviews are rare. Suppose we have substituted all the instances of $t$ with its synonyms, now $\tf(t)$ is: 

\begin{linenomath*}
    \begin{ceqn}
        \begin{align}
        \label{tfprime}
        \tfp(t,d) = \tf{(s_1,d)} + \tf{(s_2,d)} + \dots  + \tf{(s_n,d)}
        \end{align}
    \end{ceqn}
\end{linenomath*}

Since we have the document frequencies stored, we can now calculate $\idfp(t)$ by leaving the definition of $\idf(t)$ unchanged except for summing the document frequencies of all synonym terms. Considering the assumption above (i.e. there are no overlaps), this is exactly the value that we want to compute; and even if there are few, this would still be a good approximation:

\begin{linenomath*}
    \begin{ceqn}
        \begin{align}
        \label{idfprime2}
        \idfp(t)= \log{\left(\frac{N}{df(s_1) + df(s_2) + \dots + df(s_n)}\right)}
        \end{align}
    \end{ceqn}
\end{linenomath*}

We can now compute $\tfidfp(t, d)$ as $\tfp(t,d) \cdot \idfp(t)$.
\vspace{0.5em}
If an user formulates a combined query $q$, such as \emphq{beautiful location and large room}, it is translated into the triples \emphq{location is beautiful} and \emphq{room is large}. Then, given a document $d$, we can calculate the \tfidfp score for each of the triples. The sum of all scores is the total score for the query-document combination:
\begin{linenomath*}
	\begin{ceqn}
		\begin{align}
		\label{tfidfcombo}
		\tfidfp_{(q, d)}= \sum_{t \in q} \tfidfp(t,d)
		\end{align}
	\end{ceqn}
\end{linenomath*}\\
However, we need some algorithm to rank the sums of \tfidfp for each document (i.e. review). We compute it, as done before in ToKnow, with the \emph{Threshold Algorithm} \cite{Fagin2001}.\\
All the reviews are now ordered by the frequency of the searched terms, but not by intensity. In the next sections we propose an approach that expands using the scales described before.

\subsection{Expanding with Scales}
We imagine that a user that queries the system for \emphq{great breakfast} would like to have only positive comments about this feature. Likewise, if an user queries for \emphq{bad breakfast} (for instance, with the intent of checking whether the hotel they want to book has any bad reviews on such feature) they would like to only have reviews mentioning the bad aspects of this feature. Our idea is expressed by \autoref{fig:arrows}.
\begin{figure}
	\centering
	\begin{tikzpicture}
	\node (0) at (0, 0.5) {0};
	\node (P) at (6, 0.5) {Positive};
	\node (N) at (-6, 0.5) {Negative};
	\node (G) at (1.5, -0.5) {good};
	\node (Gr) at (3, -0.5) {great};
	\node (E) at (4.5, -0.5) {excellent};
	\node (B) at (-1.5, -0.5) {bad};
	\node (T) at (-3, -0.5) {terrible};
	\node (H) at (-4.5, -0.5) {horrific};
	
	\draw [line width=2.5pt, ->,colorthree, >=stealth] (0,0) -- (6,0);
	\draw [line width=2.5pt, ->,colorone, >=stealth] (0,0) -- (-6,0);
	\draw [line width=1.5pt, dotted, ->,colorthree, >=stealth] (3,-0.8) -- (6,-0.8);
	\draw [line width=1.5pt, dotted, ->,colorone, >=stealth] (-1.5,-0.8) -- (-6,-0.8);
	
	\end{tikzpicture}
	\caption{Illustration of how expansion is carried out on scales. The dotted arrows show the directions of the expansion considering if the adjective has a positive or a negative polarity.}
	\label{fig:arrows}
\end{figure}
So, we use \sentiwordnet to recognise if an adjective used in a query has a positive or a negative sentiment. In the first case, we expand only with the ones above the term, making the query \emphq{breakfast is great} produce the \emphq{breakfast is great} and \emphq{breakfast is excellent}. On the contrary, we expand only with the ones below the term, making the query \emphq{breakfast is bad} produce \emphq{breakfast is bad}, \emphq{breakfast is terrible} and \emphq{breakfast is horrific}. This concept applies for both the \sentiwordnet and the \wordvec scale.\\
We discuss in the next lines this approach with the \sentiwordnet scale. We first check if sentiment of the adjective is positive or negative. Afterwards, the approach is very similar to the one used in \autoref{intensities:sentiwordnet}, but instead of selecting a neighbourhood from all the possible words in \wordvec, we select only the terms that actually appear in our data for such feature. For an adjective with positive sentiment, we generate only the positive scale with adjectives that have a synset score higher or equal to the starting adjective. For an adjective with negative sentiment, we generate only the negative scale with adjectives that have a synset score lower or equal to the starting adjective.\\
Formulating a query with the Word2Vec scale have different results. Using the reference scale mentioned in \autoref{intensities:word2vec}, if the queried adjective is included in this scale, this is used and we would fall back in to the previously explained approach. If not, a new scale is created starting from the seed word \emph{decent}, which we picked is reasonably in the middle. The scale is generated with the approach described in \autoref{intensities:word2vec}, but instead of taking any adjective in the space, it takes the adjective for such feature that exists in the data and is closest to the \textit{expected point}. Only the more positive or more negative part of the scale for the queried adjective are considered.\\
To actually rank the reviews containing the triples generated from one or the other scale, we first do the same as in \autoref{exp:syns} to compute the \tfidfp scores for the query adjective and all the adjectives that are going to be used for the expansion. In this case, we consider all these adjectives as equivalent. Then we multiply the relevance score for each review (\tfidfp) by the rank of its used adjective in the scale. This rank is the position of the adjective in the scale and is normalised to be between 0 and 1. Now we can rank the reviews according to this value.

