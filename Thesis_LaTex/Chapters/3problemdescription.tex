\chapter{Problem Description}

Here we give a more technical overview of the \toknow system, to then present a deeper and more precise description of our problem, which will be done in the second subsection of this chapter.
\section{\toknow}
\label{problemdescription:toknow}
As previously mentioned, \toknow is the system we base our research on. The idea is to index hotel reviews using triples, where triples are three-word tuples describing a \emph{feature}, which is some particular asset of a hotel. Examples of triples for \autoref{fig:toknow1} are \triple{breakfast}{good} and \triple{location}{fantastic}.\\
The input given by users is transformed into triples and used for retrieving the most interesting hotels in whose reviews the triple is somehow mentioned.\\
\toknow exploits the StanfordNLP tool \cite{Manning2014} to extract dependency trees of sentences and then extracts triples thanks to custom-defined rules. These rules consist in finding relations between features and the sorrounding parts of a sentence.\\
Each triple is considered either a \textit{factual triple} or an \textit{opinionated triple}. The first kind is used to describe the reality of a hotel, such as \emphq{hotel in rome}, \emphq{room with view} and \emphq{hotel near train station}. The second kind, instead, expresses the writer's opinion, such as \triple{breakfast}{good} and \triple{room}{nice}.\\
With the aim of finding similarity, factual triples are not very interesting. In fact, they mostly take into account nouns, which do not have many similar words because there is only a certain number of ways in which one can express the word \emphq{hotel} in natural languages. On the contrary, in opinionated triples 94\% of the third terms are adjectives and in the case in which they are not, the triples are mostly of the type \triple{condition}{luggage} or \triple{pizza}{place}, which are not semantically interesting. Adjectives have more shades and tones, which makes them very suitable for finding similarity.\\
Opinionated triples are originated by a very common form in natural languages: \emph{adjective phrases}. They can appear in two forms; it can either be a noun plus a copula or an adjective followed by a noun.\\
In \autoref{fig:adjphr1}, we can see that \emphq{is} is a third person regular present (VBZ), \emphq{breakfast} is a noun, singular (NN) and \emphq{good} is an adjective (JJ). The \emph{cop} arrow indicates that good is the copula of the verb, and that breakfast is its nominal subject (\emph{nsubj}).\\
In \autoref{fig:adjphr2} good is the adjective and breakfast is the noun; good acts as an adjectival modifier (\emph{amod}) of the noun and modifies the meaning of breakfast adding a more positive value to it.
\begin{figure}
	\centering
	\begin{subfigure}{0.45\textwidth}
	\centering
	\includegraphics[scale=0.5, height=2cm]{adjectivephrases1.png}
	\caption{Dependencies of \textit{breakfast is good}.}
	\label{fig:adjphr1}
	\end{subfigure}
	\begin{subfigure}{0.45\textwidth}
	\centering
	\includegraphics[scale=0.5, height=2cm]{adjectivephrases2.png}
	\caption{Dependencies of \textit{good breakfast}.}
	\label{fig:adjphr2}
	\end{subfigure}
	\caption{Dependencies from CoreNLP \cite{Core2014}.}
	\label{fig:adjphr}
\end{figure}

In \toknow, both forms are translated into the opinionated triple \emphq{breakfast is good}.\\
Each of these triples is now saved with a pointer to the reviews and the hotels to which it belongs; in this way it is possible to retrieve the reviews when a triple is queried.

\section{The Problem}
\label{problemdescription}
We find interesting the need of capturing semantic sense instead of mere keywords, and thus the main objective of this work is to find synonyms or similarity and to expand keyword searches.
We consider two words \emph{synonyms}, if they are related according to the English dictionary. However, in our particular area of application that is user reviews, we might also be interested in finding \emph{similarity}, defined as the relation between some words given a specific context, which is a feature in our case. Given the data of the \toknow system, for each triple $(f, a)$ of the form \triple{feature}{adjective}, where $f$ is the feature and $a$ is the adjective, we have to find another adjective $a1$ of a triple $(f, a1)$ such that either \emph{synonym(a, a1)} or \emph{similar(a, a1)} returns true.

\begin{figure}
	\centering
	\begin{tikzpicture}[>=stealth, thick]
	\node (A) at (0,0) [draw, process, text width=3.5cm, align=flush center] 
	{staff is trustworthy};
	
	\node (B) at (-2,-2) [draw, process, text width=3.5cm,  align=flush center] 
	{staff is reliable};
	
	\node (C) at (2,-2) [draw, process, text width=3.5cm, align=flush center] 
	{staff is competent};
	
	\coordinate (z) at (0,-1);
	
	\draw[->] (A) -- (z) -| node[above] {SYNONYM} (B);
	\draw[->] (A) -- (z) -| node[above] {SIMILAR} (C);
	
	\end{tikzpicture}
	\caption{Example of synonym and similar adjective for the triple \emph{staff is trustworthy}.}
	\label{fig:synsim}
\end{figure}

\autoref{fig:synsim} shows how for the adjective \emphq{trustworthy} alone, we can consider the adjective \emphq{reliable} alone a synonym. In this particular case, we can easily see how both adjectives express the same meaning when related to the feature \emphq{staff}. However, there are cases in which a synonym of an adjective might not really be related to the feature (\autoref{fig:notsynsim}). Going back to \autoref{fig:synsim}, it can be seen how \emphq{trustworthy} and \emphq{competent} are not strictly synonyms, but they can be considered as similar when the feature \emphq{staff} is taken into account. Since determining the context in which an adjective is used is very hard, we propose to use both of these approaches.

\begin{figure}
	\centering
	\begin{tikzpicture}[>=stealth, thick]
	\node (A) at (0,0) [draw, process, text width=3cm, align=flush center] 
	{breakfast is broad};
	
	\node (B) at (-2,-2) [draw, process, text width=3.5cm, align=flush center] 
	{breakfast is spacious};
	
	\node (C) at (2,-2) [draw, process, text width=3.5cm, align=flush center] 
	{breakfast is rich};
	
	\coordinate (z) at (0,-1);
	
	\draw[->] (A) -- (z) -| node[above] {NOT SYNONYM} (B);
	\draw[->] (A) -- (z) -| node[above] {SIMILAR} (C);
	\end{tikzpicture}
	\caption{Example of not synonym and similar adjective for the triple \emph{breakfast is broad}.}
	\label{fig:notsynsim}
\end{figure}
After finding synonyms, we aim to \emph{expand queries}. This means that, starting from a triple $t$ queried by the user, we generate some triples $t_1, t_2, \dots, t_n$ where the third term is a synonym of the third term of $t$. The system will then be asked to return the hotels whose reviews mention these triples (i.e. $t, t_1, t_2, \dots, t_n$). This concept is called \emph{query expansion}. To produce these triples, we present lexical approaches (\autoref{syns:lexical}), where we try to find synonym using \wordnet, and word-vector approaches (\autoref{syns:wordvectors}), which will count in the context of the feature. We will also propose some ranking methods after the triple expansion.\\
In our work we use the TripAdvisor reviews of 2015 for the city of Rome as the underlying dataset. This only acts as a proof of concept, since it could be applied to any city. The same goes for \toknow: even though we base our research on it, our work could be used for any other system. Each of the proposed methods is written in \emph{Python 2.7} and the data is stored in \emph{JSON} files, as it is done in \toknow.