import json
import sys
import os

curp = os.path.dirname(__file__)
newpath_stats = os.path.join(curp, "../../ToKnow/tripadvisor_indexes/statistics/")
newpath_data = os.path.join(curp, "./Data/")

def convert_file(file, file_latex):
    with open(newpath_stats + file) as f:
        dictionary = json.load(f)
    d_num = {int(k):int(v) for k, v in dictionary.items()}
    max_key = max(d_num.iterkeys())
    latex = open(newpath_data + file_latex, "w")
    total_rest = 0
    around_zero = 0
    for key in range(0, max_key+1):
        if key in d_num:
            if key <= 5:
                around_zero += d_num[key]
            else:
                total_rest += d_num[key]

        if key in d_num:
            latex.write(str(key) + " " + str(d_num[key]) + "\n")
        else:
            latex.write(str(key) + " " + str(0) + "\n")
    latex.close()
    
    print file
    print 'Number of synonyms around zero: ' + str(around_zero)
    print 'Number of synonyms >= 5: ' + str(total_rest)
    print around_zero - total_rest

def convert_file_feature(file, file_latex, feature):
    with open(newpath_stats + file) as f:
        dictionary = json.load(f)
    dict_feat = dictionary[feature]
    d_num = {int(k):int(v) for k, v in dict_feat.items()}
    max_key = max(d_num.iterkeys())
    latex = open(newpath_data + file_latex, "w")

    total_rest = 0
    around_zero = 0
    for key in range(0, max_key+1):
        if key in d_num:
            if key <= 5:
                around_zero += d_num[key]
            else:
                total_rest += d_num[key]
        if key in d_num:
            latex.write(str(key) + " " + str(d_num[key]) + "\n")
        else:
            latex.write(str(key) + " " + str(0) + "\n")
    latex.close()
    print file
    print 'Number of synonyms around zero: ' + str(around_zero)
    print 'Number of synonyms >= 5: ' + str(total_rest)
    print around_zero - total_rest

def compute_average(file):
    with open(newpath_stats + file) as f:
        dictionary = json.load(f)
    avg = 0
    for key in dictionary:
        avg += key[1]

    print 'The average of synonyms for file ' + file + ' is ' + str(avg/float(len(dictionary)))

if __name__ == '__main__':

    inp = 'total.count.threshold.0.7.option.json'
    outp = 'totalcountopt.txt'

    for i in range(0, 9):
        inp_new = inp[:-4] + str(i) + '.json'
        outp_new = outp.split('.')[0] + str(i) + '.' + outp.split('.')[1]
        convert_file(inp_new, outp_new)

    inp = 'count.per.feature.threshold.0.7.option.json'
    feature = 'breakfast'
    outp = 'count' + feature + 'opt.txt'

    for i in range(0, 9):
        inp_new = inp[:-4] + str(i) + '.json'
        outp_new = outp.split('.')[0] + str(i) + '.' + outp.split('.')[1]
        convert_file_feature(inp_new, outp_new, feature)

    inp = 'average.per.feature.threshold.0.7.option.'
    for i in range(0,9):
        compute_average(inp + str(i) + '.json')

    inp = 'weighted.average.per.featurethreshold.0.7.option.'
    for i in range(0,9):
        compute_average(inp + str(i) + '.json')